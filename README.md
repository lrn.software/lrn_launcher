# lrn

Currently, the `lrn` command is merely a launcher for `lrn` style lessons.  `lrn` style lessons are defined by the `.lesson.yml` file and the `.tmuxinator.yml` file that are contained in a lesson directory/repo such as https://gitlab.com/lrn.software/parsing_text_files__python.

Lessons are done in a tmux session, and to launch this session `lrn` merely `cd`'s to the selected lesson and then runs `tmuxinator local`.

Running `tmuxinator local` in the root of a lesson directory is sufficient to launch a learning session, you technically don't need `lrn` for that.

### Getting started, 2 options:
* To get started with `lrn`, installing the software and trying the lesson, see https://gitlab.com/lrn.software/lrn_installer.

* Alternately, if you want the **shortcut**... *don't* install this launcher, just do:

  ```sh
  # first make sure you've got the dependencies installed! Then...
  git clone git@gitlab.com:lrn.software/parsing_text_files__python.git
  cd parsing_text_files__python
  tmuxinator local
  ```

### The Intention and Future
The vision is for `lrn` to do much more than launching lessons, having subcommands the way a package like `git` has `git commit`, `git checkout`, etc.

```sh
lrn browse keyword1 keyword2 # search for lessons pertaining to keywords
lrn generate # create new lesson from boilerplate/scaffolding (a command for developers to develop new lessons)
lrn restart # start lesson from the beginning with a fresh git history
```

There are some utility scripts in `parsing_text_files__python/.bin` that should probably be moved to the main `lrn` command.

`lrn` could also provide a browser for command-line help to improve the experience of `man` and `info` pages and `--help` docs.

There are probably numerous other things `lrn` should provide.
